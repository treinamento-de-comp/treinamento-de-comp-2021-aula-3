#!/bin/bash

# Create binaries directory if it does not exist
mkdir -p ../binaries

# Compile project
cd ../source
gcc main.c tests.c ITAndroidsCalculator.c -o ../binaries/main -lm

# Run project
cd ../binaries
./main